import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import {RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Dexie } from 'dexie';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { DestinosViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinosViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { ActionReducerMap, Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects'
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent} from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent} from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponent} from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinosViaje } from './models/destino-viaje.model';
import { DestinosViajesEffects, DestinosViajesState, initializeDestinosViajesState, InitMyDataAction, reducerDestinosViajes } from './models/destinos-viajes-state.model';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// appconfig
export interface AppConfig {
  apiEndPoint: string;
}
export const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
}
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// end config

// routing
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent},
  { path: 'mas-info', component: VuelosMasInfoComponent},
  { path: ':id', component: VuelosDetalleComponent}
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent},
  { path: 'login', component: LoginComponent},
  { path: 'vuelos', 
    component: VuelosComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesVuelos
  },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  }
]
// end routing

// redux init
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
}

const reducerInitialState = {
  destinos: initializeDestinosViajesState()
}
// redux fin init

// app init
export function init_app( appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializedDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) {}

  async initializedDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-segurida'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', {headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// end app init


// dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDataBase extends Dexie {
  destinos: Dexie.Table<DestinosViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDataBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl'
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}
export const db = new MyDataBase();
// end dexie db

// i18n init
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
      .where('lang')
      .equals(lang)
      .toArray()
      .then(results => {
        if ( results.length === 0 ) {
            return this.http.get<Translation[]>(`${APP_CONFIG_VALUE.apiEndPoint}/api/translation?lang=${lang}`)
            .toPromise()
            .then(apiResult => {
              db.translations.bulkAdd(apiResult);
              return apiResult;
            });
        }
        return results;
      }).then(traducciones => {
        return traducciones.map(t => ({[t.key]: t.value }));
      });
      return from(promise).pipe(flatMap(elems => from(elems)));
  }
}

function httpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
// i18n end init

@NgModule({
  declarations: [
    AppComponent,
    DestinosViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinosViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot(reducers, { initialState: reducerInitialState,
        runtimeChecks: { 
            strictStateImmutability: false, 
            strictActionImmutability: false,
        } 
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // retain last 25 states
      logOnly: environment.production
    }),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (httpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule
  ],
  providers: [
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    MyDataBase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
