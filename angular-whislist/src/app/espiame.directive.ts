import { Directive, OnDestroy, OnInit } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective implements OnInit, OnDestroy {

  static nextId = 0;
  log = (msg) => console.log(`Evento #${EspiameDirective.nextId++} ${msg}`);
  constructor() { }

  ngOnInit() { console.log(this.log('###################*********** onInit'));}

  ngOnDestroy() {console.log(this.log('###################*********** onDestroy'));}

}
