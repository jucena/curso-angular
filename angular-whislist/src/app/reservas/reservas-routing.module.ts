import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservasDetallesComponent } from './reservas-detalles/reservas-detalles.component';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';

const routes: Routes = [
  {path: 'reservas', component: ReservasListadoComponent},
  {path: 'reservas/:id', component: ReservasDetallesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
