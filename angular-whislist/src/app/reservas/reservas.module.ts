import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasDetallesComponent } from './reservas-detalles/reservas-detalles.component';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasApiClientService } from './reservas-api-client.service';


@NgModule({
  declarations: [ReservasDetallesComponent, ReservasListadoComponent],
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],
  providers: [ ReservasApiClientService ]
})
export class ReservasModule { }
