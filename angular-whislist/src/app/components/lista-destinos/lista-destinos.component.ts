import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinosViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {

  // @Output() onItemAdded: EventEmitter<DestinosViaje>
  updates: string[];
  all: DestinosViaje[];

  constructor(public destinoApiClient: DestinosApiClient, private store: Store<AppState>) {
    // this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select( state => state.destinos.favorito )
      .subscribe( fav => {
        if ( fav != null ) {
          this.updates.push('se ha elegido a' + fav.nombre);
        }
      });
      this.store.select( state => state.destinos.items ).subscribe( items => this.all = items );
  }

  ngOnInit(): void {
  }

  agregado(destino: DestinosViaje ) {
      this.destinoApiClient.add(destino);
  }

  elegido(destino: DestinosViaje) {
    this.destinoApiClient.elegir(destino);
  }

}
