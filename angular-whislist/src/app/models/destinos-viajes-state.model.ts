import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { DestinosViaje } from "./destino-viaje.model";

// estado
export interface DestinosViajesState {
    items: DestinosViaje[];
    loading: boolean;
    favorito: DestinosViaje
}

export const initializeDestinosViajesState = function() {
    return {
        items: [],
        loading: false,
        favorito: null
    }
}

// acciones
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    VOTE_RESET = '[Destinos Viajes] Vote Reset',
    MY_DATA = '[Destinos Viajes] Init my Data'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinosViaje){}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinosViaje){}
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinosViaje){}
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinosViaje){}
}

export class VoteResetAction implements Action {
    type = DestinosViajesActionTypes.VOTE_RESET;
    constructor(public destino: DestinosViaje){}
}

export class InitMyDataAction implements Action {
    type = DestinosViajesActionTypes.MY_DATA;
    constructor(public destinos: string[]){}
}

export type DestinosViajesActions = NuevoDestinoAction | 
                                    ElegidoFavoritoAction |
                                    VoteDownAction |
                                    VoteUpAction |
                                    VoteResetAction |
                                    InitMyDataAction;


//reducers
export function reducerDestinosViajes( 
        state: DestinosViajesState, 
        action: DestinosViajesActions
        ): DestinosViajesState {

    switch ( action.type ) {
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state, items: [...state.items, ( action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            let items = [...state.items ];
            items = items.map( x => { 
                x.setSelected(false);
                return x;
            });
            const favorito: DestinosViaje = (action as ElegidoFavoritoAction).destino;
            favorito.setSelected(true);
            return {
                ...state, items,favorito
            }
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            const d: DestinosViaje = (action as VoteUpAction).destino;
            d.voteUp();
            return {
                ...state, 
                items: state.items.map(i=>{ 
                   if (i.nombre==d.nombre) {
                       return d;
                   } else {
                       return i;
                   }
                })
            }
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
            const d: DestinosViaje = (action as VoteDownAction).destino;
            d.voteDown();
            return {
                ...state,
                items: state.items.map(i=>{ 
                    if (i.nombre==d.nombre) {
                        return d;
                    } else {
                        return i;
                    }
                 }) 
            }
        }
        case DestinosViajesActionTypes.VOTE_RESET: {
            const d: DestinosViaje = (action as VoteDownAction).destino;
            d.voteReset();
            return {
                ...state,  
            }
        }
        case DestinosViajesActionTypes.MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state, items: destinos.map(d=> new DestinosViaje(d,''))
            }
        }
        default: return state;
    }
}

// effects
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction( action.destino))
    );
    constructor(private actions$: Actions){}
}
