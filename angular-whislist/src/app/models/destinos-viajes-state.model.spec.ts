import { DestinosViaje } from "./destino-viaje.model";
import { DestinosViajesState, ElegidoFavoritoAction, initializeDestinosViajesState, InitMyDataAction, NuevoDestinoAction, reducerDestinosViajes, VoteDownAction, VoteResetAction, VoteUpAction } from "./destinos-viajes-state.model";

describe('reducerDestinosViajes', ()=> {
    it('should reduce init data', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1','destino 2']);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item add', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction( new DestinosViaje('barcelona','url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items[0].nombre).toEqual('barcelona')
    });

    it('should reduce new item favorite', () => {
        // NuevoDestino
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const actionNewDestinoViaje: NuevoDestinoAction = new NuevoDestinoAction(new DestinosViaje('barcelona','url'));
        reducerDestinosViajes(prevState,actionNewDestinoViaje);
        // favorite
        const action: ElegidoFavoritoAction = new ElegidoFavoritoAction( new DestinosViaje('barcelona','url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.favorito.selected).toBeTrue();
    });

    it('should reduce voteUp DestinoViaje clicked', () => {
         // NuevoDestino
         const prevState: DestinosViajesState = initializeDestinosViajesState();
         const actionNewDestinoViaje: NuevoDestinoAction = new NuevoDestinoAction(new DestinosViaje('barcelona','url'));
         let newStateDestino = reducerDestinosViajes(prevState,actionNewDestinoViaje);
        debugger
        expect(newStateDestino.items[0].votes).toEqual(0);
        const action: VoteUpAction = new VoteUpAction( new DestinosViaje('barcelona','url'));
        newStateDestino = reducerDestinosViajes(newStateDestino, action);
        expect(newStateDestino.items[0].votes).toEqual(1);
    });

    it('should reduce voteDown DestinoViaje clicked', () => {
        // NuevoDestino
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const actionNewDestinoViaje: NuevoDestinoAction = new NuevoDestinoAction(new DestinosViaje('barcelona','url'));
        let newStateDestino = reducerDestinosViajes(prevState,actionNewDestinoViaje);
        
       expect(newStateDestino.items[0].votes).toEqual(0);
       const action: VoteDownAction = new VoteDownAction( new DestinosViaje('barcelona','url'));
       newStateDestino = reducerDestinosViajes(newStateDestino, action);
       expect(newStateDestino.items[0].votes).toEqual(-1);
   });

   it('should reduce voteReset to 0 DestinoViaje clicked', () => {
        // NuevoDestino
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const actionNewDestinoViaje: NuevoDestinoAction = new NuevoDestinoAction(new DestinosViaje('barcelona','url'));
        let newStateDestino = reducerDestinosViajes(prevState,actionNewDestinoViaje);

        expect(newStateDestino.items[0].votes).toEqual(0);
        const action: VoteResetAction = new VoteResetAction( new DestinosViaje('barcelona','url'));
        newStateDestino = reducerDestinosViajes(newStateDestino, action);
        expect(newStateDestino.items[0].votes).toEqual(0);
    });

});