export class DestinosViaje {
    selected;
    servicios: string[];
    public votes: number;

    constructor(public nombre: string,public imagenUrl: string) {
        this.servicios = ['pileta', 'desayuno'];
        this.votes = 0;
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(value: boolean): void {
        this.selected = value;
    }

    voteUp(): void {
        this.votes++;
    }

    voteDown(): void {
        this.votes--
    }

    voteReset(): void {
        this.votes = 0;
    }

}