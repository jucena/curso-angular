import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { DestinosViaje } from './destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {

	destinos: DestinosViaje[] = [];

	constructor( private store: Store<AppState>, 
				 @Inject(forwardRef(()=> APP_CONFIG))private config: AppConfig,
				 private http: HttpClient) {

		this.store.select(state=> state.destinos).
		subscribe(data=> {
			this.destinos = data.items;
		});
	}
	add(destino:DestinosViaje){

		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-segurida'});
		const req = new HttpRequest('POST', `${this.config.apiEndPoint}/my`,{nuevo: destino.nombre}, {headers});
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch( new NuevoDestinoAction(destino));
				const myDb = db;
				myDb.destinos.add(destino);
				myDb.destinos.toArray().then(destinos=>console.log(destinos));
			}
		});
	}

	getAll(): DestinosViaje[] {
		return this.destinos;
	}
	
	elegir(destino: DestinosViaje) {
		this.store.dispatch( new ElegidoFavoritoAction(destino));
	}

	getById(id: string): DestinosViaje {
		return this.destinos[0];
	}
}
