describe('ventana principal', () => {
    it('tiene encabezado correcto y en espanol por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-whislist');
        cy.get('h1 b').should('contain', 'HOLA es');
    });

    it('tiene titulo correcto', () => {
        cy.visit('http://localhost:4200');
        cy.get('h1').should('contain','Ruteo Simple');
    });

    it('tiene link to home', () => {
        cy.visit('http://localhost:4200');
        cy.get('div a').should('have.attr', 'href', '/home')
    });
});